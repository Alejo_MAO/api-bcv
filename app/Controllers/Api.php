<?php

namespace App\Controllers;

class Api extends BaseController
{
  public function index()
  {
    ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);
    include APPPATH."ThirdParty/simplehtmldom/simple_html_dom.php";
    $data = [];
    //header("Content-Type: text/plain");
    $html = file_get_html('http://www.bcv.org.ve');
    $currency['data']['euro']['value'] = trim($html->find('div[id="euro"] .field-content .recuadrotsmc .centrado strong',0)->plaintext);
    $currency['data']['euro']['iso'] = trim($html->find('div[id="euro"] .field-content .recuadrotsmc span',0)->plaintext);

    $currency['data']['yuan']['value'] = trim($html->find('div[id="yuan"] .field-content .recuadrotsmc .centrado strong',0)->plaintext);
    $currency['data']['yuan']['iso'] = trim($html->find('div[id="yuan"] .field-content .recuadrotsmc span',0)->plaintext);
    
    $currency['data']['lira']['value'] = trim($html->find('div[id="lira"] .field-content .recuadrotsmc .centrado strong',0)->plaintext);
    $currency['data']['lira']['iso'] = trim($html->find('div[id="lira"] .field-content .recuadrotsmc span',0)->plaintext);
    
    $currency['data']['ruble']['value'] = trim($html->find('div[id="rublo"] .field-content .recuadrotsmc .centrado strong',0)->plaintext);
    $currency['data']['ruble']['iso'] = trim($html->find('div[id="rublo"] .field-content .recuadrotsmc span',0)->plaintext);
    
    $currency['data']['dollar']['value'] = trim($html->find('div[id="dolar"] .field-content .recuadrotsmc .centrado strong',0)->plaintext);
    $currency['data']['dollar']['iso']= trim($html->find('div[id="dolar"] .field-content .recuadrotsmc span',0)->plaintext);

    header("Content-Type: application/json; charset=UTF-8");
    if (isset($_GET['get']) && !empty($_GET['get'])) {
      if(isset($currency['data'][$_GET['get']])){
        $response['status'] = 200;
        $response['data'][$_GET['get']] = $currency['data'][$_GET['get']];
      }else{
        $response['status'] = 500;
        $response['data'] = null;
      }
      
      echo json_encode($response,JSON_NUMERIC_CHECK);
    } else{
      $currency['status'] = 200;
      echo json_encode($currency,JSON_NUMERIC_CHECK);
    }
  }
}

<?php
namespace App\Models;
use CodeIgniter\Model;

class AdminModel extends Model
{
	protected $table = 'admin';
	protected $primaryKey = 'ID';

	protected $useAutoIncrement = true;

	protected $returnType = 'array';
	protected $useSoftDeletes = false;

	protected $allowedFields = [
		'name',
		'phone',
		'email',
		'app_name',
		'app_logo',
		'user',
		'password'
	];

	protected $useTimestamps = false;
	protected $createdField = '';
	protected $updatedField = '';
	protected $deletedField = '';

	protected $validationRules = [];
	protected $validationMessages = [];
	protected $skipValidation = false;
}

?>
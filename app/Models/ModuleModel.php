<?php
namespace App\Models;
use CodeIgniter\Model;

class ModuleModel extends Model
{
	protected $table = 'sys_modules';
	protected $primaryKey = 'controller';

	protected $useAutoIncrement = false;

	protected $returnType = 'array';
	protected $useSoftDeletes = false;

	protected $allowedFields = [
		'controller',
		'name',
		'icon',
	];

	protected $useTimestamps = false;
	protected $createdField = '';
	protected $updatedField = '';
	protected $deletedField = '';

	protected $validationRules = [];
	protected $validationMessages = [];
	protected $skipValidation = false;
}

?>
<?php
namespace App\Models;
use CodeIgniter\Model;

class MenuModel extends Model
{
	protected $table = 'sys_menu';
	protected $primaryKey = 'ID';

	protected $useAutoIncrement = true;

	protected $returnType = 'array';
	protected $useSoftDeletes = false;

	protected $allowedFields = [
		'_business',
		'role',
		'type',
		'modules',
	];

	protected $useTimestamps = false;
	protected $createdField = '';
	protected $updatedField = '';
	protected $deletedField = '';

	protected $validationRules = [];
	protected $validationMessages = [];
	protected $skipValidation = false;
}

?>
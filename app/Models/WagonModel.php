<?php
namespace App\Models;
use CodeIgniter\Model;

class WagonModel extends Model
{
	protected $table = 'wagon';
	protected $primaryKey = 'ID';

	protected $useAutoIncrement = true;

	protected $returnType = 'array';
	protected $useSoftDeletes = false;

	protected $allowedFields = [
		'COD',
		'name'
	];

	protected $useTimestamps = false;
	protected $createdField = '';
	protected $updatedField = '';
	protected $deletedField = '';

	protected $validationRules = [];
	protected $validationMessages = [];
	protected $skipValidation = false;

	public function GetAll($query=[],$pagination=null)
	{
		foreach ($query as $key => $value) {
		  if (is_string($key) and isset($value['fields'])) {
		    $this->groupStart();
		    if (is_array($value['fields']) and is_string($value['value'])) {
		      $st = 0;
		      foreach ($value['fields'] as $field) {
		        if ($st == 0) {
		          $this->like($field,$value['value']);
		        }else{
		          $this->orLike($field,$value['value']);
		        }
	        	$st++;
		      }
		    }
		    if (is_string($value['fields']) and is_array($value['value']) ) {
		      $st = 0;
		      foreach ($value['value'] as $k=>$v) {
		        if ($st == 0) {
		          $this->like("{$value['fields']}","{$v}");
		        }else{
		          $this->orLike("{$value['fields']}","{$v}");
		        }
	        	$st++;
		      }
		    }
		    if (is_string($value['fields']) and is_string($value['value']) ) {
		      $this->like($value['fields'],$value['fields']);
		    }
		    $this->groupEnd();
		  }elseif (is_string($value)) {
		    $this->groupStart();
		    $this->where($key,$value);
		    $this->groupEnd();
		  }elseif (is_array($value)) {
		    $this->groupStart();
		    $st = 0;
		    foreach ($value as $field => $val) {
		      if ($st == 0) {
		        $this->where($key,$val);
		      }else{
		        $this->orWhere($key,$val);
		      }
		      $st++;
		    }
		    $this->groupEnd();
		  }
		}
		if (!empty($pagination)) {
			$this->limit($pagination['per_page'],$pagination['page_firts_result']);
		}
		return $this->find();
	}
	public function GetCountAll($query='')
	{
		$this->select('COUNT(*) as total');
		foreach ($query as $key => $value) {
		  if (is_string($key) and isset($value['fields'])) {
		    $this->groupStart();
		    if (is_array($value['fields'])) {
		      $st = 0;
		      foreach ($value['fields'] as $field) {
		        if ($st == 0) {
		          $this->like($field,$value['value']);
		        }else{
		          $this->orLike($field,$value['value']);
		        }
		        $st++;
		      }
		    }
		    if (is_string($value['fields'])) {
		      $this->like($value['fields'],$value['value']);
		    }
		    $this->groupEnd();
		  }elseif (is_string($value)) {
		    $this->groupStart();
		    $this->where($key,$value);
		    $this->groupEnd();
		  }elseif (is_array($value)) {
		    $this->groupStart();
		    $st = 0;
		    foreach ($value as $field => $val) {
		      if ($st == 0) {
		        $this->where($key,$val);
		      }else{
		        $this->orWhere($key,$val);
		      }
		      $st++;
		    }
		    $this->groupEnd();
		  }
		}
		
		return $this->first();
	}
}

?>
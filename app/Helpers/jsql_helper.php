<?php
if(!function_exists("json_to_sql")){
  function json_to_sql($values=[],$tbl=null){
    $table = (!empty($tbl))?"{$tbl}.":"";
    $parent = "$.{$table}";
    $json = [];
    foreach ($values as $key => $value) {
      $json[] = "'{$parent}{$key}','{$value}'";
    }
    $sql = implode(',', $json);
    return $sql;
  }
}
?>
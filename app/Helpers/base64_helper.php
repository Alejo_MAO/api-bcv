<?php
if(!function_exists("save_file")){
  function save_file($filename){
    if (is_array($filename['name'])) {
      $file = [];
      for ($key=0;$key<count($filename['name']);$key++) {
        $path = $filename['tmp_name'][$key];
        $data = file_get_contents($path);
        $encode = base64_encode($data);
        $file[$key] = "data:{$filename['type'][$key]};base64,{$encode}";
      }
    }else{
      $path = $filename['tmp_name'];
      $data = file_get_contents($path);
      $encode = base64_encode($data);
      $file[$key] = "data:{$filename['type']};base64,{$encode}";
    }
    return $file;
  }
}
?>